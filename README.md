

````
 * TI-73 / TI-73 Explorer
 * TI-76.fr
 * TI-81
 * TI-82
 * TI-82 STATS / TI-82 STATS.fr
 * TI-83
 * TI-83 Plus / TI-83 Plus Silver Edition / TI-83 Plus.fr
 * TI-84 Plus / TI-84 Plus Silver Edition / TI-84 pocket.fr
 * TI-85
 * TI-86
````


````
Tilem2

http://lpg.ticalc.org/prj_tilem/
GTK+ TI Z80 calculator emulator

TilEm is an emulator and debugger for Texas Instruments' Z80-based graphing calculators. It can emulate any of the following calculator models: * TI-73 / TI-73 Explorer * TI-76.fr * TI-81 * TI-82 * TI-82 STATS / TI-82 STATS.fr * TI-83 * TI-83 Plus / TI-83 Plus Silver Edition / TI-83 Plus.fr

    TI-84 Plus / TI-84 Plus Silver Edition / TI-84 pocket.fr *

TI-85 * TI-86 TilEm fully supports all known versions of the above calculators (as of 2012), and attempts to reproduce the behavior of the original calculator hardware as faithfully as possible. In addition, TilEm can emulate the TI-Nspire's virtual TI-84 Plus mode. This is currently experimental, and some programs may not work correctly.

TilEm runs on the X Window System on GNU/Linux and other Unix- like platforms, as well as on Microsoft Windows, and any other platform supported by the GTK+ library. In addition to the emulation, TilEm 2 provide a lot of extra features, such as: * Fully featured debugger * Grabbing screenshots and recording gif (animations) * Virtual linking (through libticables) * Flash writing and erasing * Application and OS loading * Scripting using macros



Download

http://ftp.debian.org/debian/pool/main/t/tilem/tilem_2.0.orig.tar.bz2




Categories

    Runs-on:GNU/Linux

````



![](medias/ti76.png)
![](medias/ti81.png)
![](medias/ti82-stats.png)
![](medias/ti83.png)
![](medias/ti83p.png)
![](medias/ti84p.png)
![](medias/ti86.png)


## Windows 

Wabbitemu on Win. 

![](medias/wabbitemu-1-win7.png)

## TI85 skin

Running on raspberry pi 

![](calc/ti85/ti85-tilem2-ok.png)



